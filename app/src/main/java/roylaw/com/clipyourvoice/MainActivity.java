package roylaw.com.clipyourvoice;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements RecognitionListener {

    private final int REQ_CODE_SPEECH_INPUT = 100;
    public final String TAG = "MainActivity";

    private TextView mInitialTextView;
    private RelativeLayout mBackgroundLayout;
    private ImageButton mMicButton;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SpeechRecognizer mSpeechRecognizer;
    private Intent mRecognizerIntent;
    private boolean isRecording = false;
    private ArrayList<String> mDataSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mInitialTextView = (TextView) findViewById(R.id.initial_text_view);
        mBackgroundLayout = (RelativeLayout) findViewById(R.id.background_layout);
        mMicButton = (ImageButton) findViewById(R.id.mic_button);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mDataSet = new ArrayList<>();

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new ListAdapter(mDataSet, this);
        mRecyclerView.setAdapter(mAdapter);

        mRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//        mRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "en");
        mRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, Locale.getDefault());
        mRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                this.getPackageName());
        mRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        mRecognizerIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);

        mMicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                promptSpeechInput();
                if (isRecording) {
                    stopRecord();
                } else {
                    startRecord();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mSpeechRecognizer != null) {
            mSpeechRecognizer.destroy();
            Log.d(TAG, "mSpeechRecognizer destroyed");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void startRecord() {
        mInitialTextView.setVisibility(View.GONE);
        if (mSpeechRecognizer == null) {
            mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
            mSpeechRecognizer.setRecognitionListener(this);
        }
        mSpeechRecognizer.startListening(mRecognizerIntent);
    }

    private void stopRecord() {
        mSpeechRecognizer.stopListening();
    }

    private void changeRecordStatus(boolean isRecording) {
        if (isRecording) {
            mBackgroundLayout.setBackgroundResource(R.color.colorRecording);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorRecordingDark));
            }
            mMicButton.setImageResource(R.drawable.ic_stop_white_48dp);
        } else {
            mBackgroundLayout.setBackgroundResource(R.color.colorPrimary);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            }
            mMicButton.setImageResource(R.drawable.ic_mic_white_48dp);
        }
    }

    /** speech recognizer */

    @Override
    public void onReadyForSpeech(Bundle params) {
        Log.d(TAG, "onReadyForSpeech");
        isRecording = true;
        changeRecordStatus(isRecording);
    }

    @Override
    public void onBeginningOfSpeech() {
        Log.d(TAG, "onBeginningOfSpeech");
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        Log.d(TAG, "onRmsChanged");

    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.d(TAG, "onBufferReceived");

    }

    @Override
    public void onEndOfSpeech() {
        Log.d(TAG, "onEndOfSpeech");
    }

    @Override
    public void onError(int error) {
        Log.d(TAG, "onError");
        isRecording = false;
        changeRecordStatus(isRecording);
    }

    @Override
    public void onResults(Bundle results) {
        Log.d(TAG, "onResults");
        isRecording = false;
        changeRecordStatus(isRecording);

        if (results != null) {
            ArrayList<String> resultStrings = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            if (resultStrings != null && resultStrings.size() > 0) {

                for (int i = 0; i < resultStrings.size(); i++) {
                    Log.d(TAG, "result " + i + ": " + resultStrings.get(i));
                }

                mDataSet.clear();
                mDataSet.addAll(resultStrings);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onPartialResults(Bundle partialResults) {
        Log.d(TAG, "onPartialResults");
        if (partialResults != null) {
            ArrayList<String> resultStrings = partialResults.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            if (resultStrings != null && resultStrings.size() > 0) {

                for (int i = 0; i < resultStrings.size(); i++) {
                    Log.d(TAG, "result " + i + ": " + resultStrings.get(i));
                }

                mDataSet.clear();
                mDataSet.addAll(resultStrings);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onEvent(int eventType, Bundle params) {
        Log.d(TAG, "onEvent");
    }
}
