package roylaw.com.clipyourvoice;

import android.app.Application;

import com.firebase.client.Firebase;

/**
 * Created by Roy on 29/2/2016.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // setup Firebase
        Firebase.setAndroidContext(this);
    }
}
